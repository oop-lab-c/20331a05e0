#include<iostream>
using namespace std;
class Abs{
    public:
    virtual void show() =0;
};
class Derived :Abs{
    public:
    void show()
    {
        cout<<"show method overrided"<<endl;
    }
};
int main()
{
    Derived obj;
    obj.show();
    return 0;
}