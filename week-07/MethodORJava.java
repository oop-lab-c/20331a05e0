public class MethodORJava {
    public void displayMsg(int a,int b)
    {
        System.out.println("base class " + (a+b));
    }
}
class Derived extends MethodORJava{
    public void displayMsg(int a,int b){
        //super.displayMsg(2,3);
        System.out.println("Derived class " + a*b);
    }
    public static void main(String args[])
    {
        Derived obj = new Derived();
        obj.displayMsg(2,3);
    }
}
